import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {
  }

  getAll(start: number, count: number): Observable<any> {
    return this.http.get(`http://localhost:8080/product/getAll?start=${start}&count=${count}`);
  }
}
