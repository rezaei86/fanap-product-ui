import {Component, OnInit} from '@angular/core';
import {ProductService} from './product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'productUI';
  public productList = [];
  public pageSize = 10;
  public page = 0;

  constructor(private productService: ProductService) {
  }

  ngOnInit(): void {
    this.loadAll(this.page, this.pageSize);
  }

  private loadAll(start: number, count: number) {
    this.productService.getAll(start, count).subscribe(res => {
      this.productList = res;
    });

  }

  nextPage() {
    this.page++;
    this.loadAll(this.page * this.pageSize + 1, this.pageSize);
  }

  previousPage() {
    this.page--;
    if (this.page >= 0) {
      this.loadAll(this.page * this.pageSize + 1, this.pageSize);
    }
  }
}
