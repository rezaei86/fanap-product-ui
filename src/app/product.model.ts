export class ProductModel {
  constructor(public id?: number,
              public  isDeleted?: boolean,
              public  osId?: number,
              public  averageRateIndex?: number,
              public  iconThumbNail?: string,
              public  downLoadLink?: string,
              public  osTypeId?: number,
              public  osTypeName?: string,
              public  appCategoryName?: string,
              public  appCategoryId?: string,
              public  targetSdk?: string,
              public  minSdk?: string,
              public  versionCode?: string,
              public  versionName?: string,
              public  fileSize?: number,
              public  packageName?: string,
              public  title?: string,
              public  developer?: string,
              public  publishState?: string,
              public  shortDescription?: string,
              public  osName?: string) {
  }
}
